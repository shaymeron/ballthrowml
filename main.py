import pygame
import math
from pygame.locals import *
import numpy
import pygame.freetype
# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255,69,0)
BORANGE = (250,131,32)
GREY = (169,169,169)

# Define some metrics
SCREEN_WIDTH = 700
SCREEN_HEIGHT = 500
SCREEN_SIZE = math.sqrt((SCREEN_WIDTH**2)+(SCREEN_HEIGHT**2))
BALL_RADIUS = int(22)
HOOP_RADIUS = int(BALL_RADIUS*numpy.sqrt(100/27))
BOARD_HEIGHT = 120
BOARD_WIDTH = 72
HOOP_Y = 120
HOOP_X = SCREEN_WIDTH-130
BOARD_X = HOOP_X+2*HOOP_RADIUS
BOARD_Y = HOOP_Y-BOARD_HEIGHT+(BOARD_HEIGHT*12/42)
BOARD_Y_BOTTOM = BOARD_Y+BOARD_HEIGHT
Nlines = 4

class Ball:
    """
    Class to keep track of a ball's location and vector.
    """
    def __init__(self):
        self.x = 0
        self.y = 0
        self.prev_y = 0
        self.vx = 0
        self.vy = 0
        self.ax = 0
        self.ay = -9.8 #gravity


    def updateVx(self, dt):
        self.vx = self.vx + self.ax * dt
        return self.vx

    def updateVy(self, dt):
        self.vy = self.vy + self.ay * dt
        return self.vy

    def updateX(self, dt):
        self.x = int(self.x + 0.5*(self.vx + self.updateVx(dt))*dt)
        return self.x

    def updateY(self, dt):
        self.prev_y = self.y
        self.y = int(self.y - 0.5*(self.vy + self.updateVy(dt))*dt)
        return self.y

    def isTouchedBoard(self):
        if (self.x+BALL_RADIUS)>=(BOARD_X) and \
                (self.y<BOARD_Y_BOTTOM and self.y>BOARD_Y):
            return True
        return False

    def touchBoardProccess(self):
        if self.isTouchedBoard():
            self.vx *=-1

    def isScore(self):
        x_bool = ((self.x+BALL_RADIUS)<(HOOP_X+2*HOOP_RADIUS)) and ((self.x-BALL_RADIUS)>HOOP_X)
        y_bool = (self.prev_y<=HOOP_Y) and (self.y>=HOOP_Y)
        if (x_bool) and (y_bool):
            return True
        return False

    def isStartMoving(self):
        return self.vx or self.vy

class My_events:
    def __init__(self):
        self.done = 0
        self.Nclicks = 0
        self.draw_dash_line = False
        self.start_throw = False

def draw_hoop(screen):
    hoopWidth = 10
    pygame.draw.line(screen, RED, [HOOP_X,HOOP_Y], [HOOP_X+2*HOOP_RADIUS,HOOP_Y],hoopWidth)
    for line in range(0,Nlines+1):
        if line<=Nlines/2:
            pygame.draw.line(screen, WHITE, [HOOP_X+2*HOOP_RADIUS*line/Nlines,HOOP_Y+hoopWidth/2], [HOOP_X+2*HOOP_RADIUS*(line+1)/Nlines,HOOP_Y+hoopWidth/2+60],4)
        if line>=Nlines/2:
            pygame.draw.line(screen, WHITE, [HOOP_X+2*HOOP_RADIUS*line/Nlines,HOOP_Y+hoopWidth/2], [HOOP_X+2*HOOP_RADIUS*(line-1)/Nlines,HOOP_Y+hoopWidth/2+60],4)

def draw_board(screen):
    pygame.draw.rect(screen,BLACK,(BOARD_X,BOARD_Y,8,BOARD_HEIGHT))
    pygame.draw.rect(screen,BLACK,(BOARD_X+10,BOARD_Y+20,8,SCREEN_HEIGHT-BOARD_Y-20))

def draw_dashed_line(surf, color, start_pos, end_pos, width=1, dash_length=10):
    x1, y1 = start_pos
    x2, y2 = end_pos
    dl = dash_length

    if (x1 == x2):
        y_coords = [y for y in range(y1, y2, dl if y1 < y2 else -dl)]
        x_coords = [x1] * len(y_coords)
    elif (y1 == y2):
        x_coords = [x for x in range(x1, x2, dl if x1 < x2 else -dl)]
        y_coords = [y1] * len(x_coords)
    else:
        a = abs(x2 - x1)
        b = abs(y2 - y1)
        c = round(math.sqrt(a**2 + b**2))
        dx = dl * a / c
        dy = dl * b / c

        x_coords = [x for x in numpy.arange(x1, x2, dx if x1 < x2 else -dx)]
        y_coords = [y for y in numpy.arange(y1, y2, dy if y1 < y2 else -dy)]

    next_coords = list(zip(x_coords[1::2], y_coords[1::2]))
    last_coords = list(zip(x_coords[0::2], y_coords[0::2]))
    for (x1, y1), (x2, y2) in zip(next_coords, last_coords):
        start = (round(x1), round(y1))
        end = (round(x2), round(y2))
        pygame.draw.line(surf, color, start, end, width)

def draw_static_shapes(screen,img,score,GAME_FONT):
    screen.blit(img, [0, 0])
    draw_hoop(screen)
    draw_board(screen)
    textsurface = GAME_FONT.render("SCORE: %d" % (score), False, WHITE)
    screen.blit(textsurface, (int(SCREEN_WIDTH / 2.5), 30))

def create_ball(x, y):
    """
    Function to make a new ball.
    """
    ball = Ball()
    # Starting position of the ball.
    # Take into account the ball size so we don't spawn on the edge.
    ball.x = int(x)
    ball.y = int(y)

    return ball
def get_axes_velocity(velocity,angle):
    vx = (velocity * math.cos((angle)))
    vy = (velocity * math.sin((angle)))
    return  vx,vy
def main(pos_vec=[None],velocity_vec=[None],angle_vec=[None]):
    """
    This is our main program.
    """
    t = 0
    score = 0
    ball_count = 0
    my_events = My_events()
    pygame.init()
    # Set the height and width of the screen
    size = [SCREEN_WIDTH, SCREEN_HEIGHT]
    screen = pygame.display.set_mode(size)
    pygame.display.set_caption("ThrowToBasket-DL")
    pygame.font.init()
    GAME_FONT  = pygame.font.SysFont('Comic Sans MS', 30)
    # Set the screen background
    BckImage = pygame.image.load('court.jpg').convert()
    # Used to manage how fast the screen updates
    clock = pygame.time.Clock()
    # -------- Main Program Loop -----------
    # Loop until the user clicks the close button.
    pos = pos_vec[ball_count]
    velocity = velocity_vec[ball_count]
    angle = angle_vec[ball_count]
    while not my_events.done:
        if pos and 'ball' not in locals():
            ball = create_ball(pos[0], pos[1])
        if velocity and angle and not ball.isStartMoving():
            ball.vx, ball.vy = get_axes_velocity(velocity, angle)
            my_events.start_throw = True
        # --- Event Processing
        for event in pygame.event.get():
            if event.type == pygame.MOUSEMOTION:
                if my_events.draw_dash_line:
                    draw_static_shapes(screen, BckImage, score, GAME_FONT)
                    x,y = pygame.mouse.get_pos()
                    draw_dashed_line(screen, WHITE, [ball.x,ball.y],[x,y])
            if event.type == pygame.MOUSEBUTTONUP:
                if 'ball' not in locals():
                    pos = pygame.mouse.get_pos()
                    my_events.draw_dash_line = True
                elif (not angle) and (not velocity):
                    x,y = pygame.mouse.get_pos()
                    x = float(x)
                    y = float(y)
                    angle = numpy.arctan((y-float(ball.y))/(float(ball.x)-x))
                    velocity = 450*math.hypot((float(ball.x)-x),(y-float(ball.y)))/SCREEN_SIZE
                    if x>=ball.x:
                        velocity*=-1
                    print("velocity: %f, angle: %f " % (velocity,angle))
                    my_events.draw_dash_line = False


            elif event.type == pygame.QUIT:
                my_events.done = True
        # --- Drawing
        # Draw the ball
        if not my_events.draw_dash_line:
            draw_static_shapes(screen, BckImage, score, GAME_FONT)
        if 'ball' in locals():
            #Draw the ball
            pygame.draw.circle(screen, BORANGE, [ball.x, ball.y], BALL_RADIUS)
            if ball.isStartMoving():
                ball.touchBoardProccess()
                if ball.isScore():
                    score+=1
                ball.updateX(0.15)
                ball.updateY(0.15)
            if ball.y>SCREEN_HEIGHT or ball.x>SCREEN_WIDTH:
                del ball
                try:
                    ball_count+=1
                    pos = pos_vec[ball_count]
                    velocity = velocity_vec[ball_count]
                    angle = angle_vec[ball_count]
                except:
                    pos,velocity,angle = None,None,None

        # Limit to 90 frames per second
        clock.tick(90)
        # Go ahead and update the screen with what we've drawn.
        pygame.display.flip()

    # Close everything down
    pygame.quit()

if __name__ == "__main__":
    Velocity_range = [40, 110]
    Angle_range = [0, numpy.pi/2]
    X_range = [0, HOOP_X]
    Y_range = [0, HOOP_Y]
    velocity_vec = numpy.random.uniform(Velocity_range[0],Velocity_range[1],1000)
    angle_vec = numpy.random.uniform(Angle_range[0],Angle_range[1],1000)
    x_vec = numpy.random.uniform(X_range[0],X_range[1],1000)
    y_vec = numpy.random.uniform(Y_range[0],Y_range[1],1000)
    pos_vec = [[x, y] for x,y in zip(x_vec,y_vec)]

    main(pos_vec,velocity_vec,angle_vec)



